#!/usr/bin/env bash

INPUT_FILE=test_FTAG1.pool.root
if [[ ! -f $INPUT_FILE ]] ; then
    curl https://dguest-ci.web.cern.ch/dguest-ci/ci/ftag/dumper/21.2.80/DAOD_FTAG1.small.pool.root > ${INPUT_FILE}
fi

# run the script
CFG_FILE=configs/single-b-tag/EMPFlow.json
dump-single-btag -c $(dirname ${BASH_SOURCE[0]})/${CFG_FILE} $INPUT_FILE
