#include "BTagInputChecker.hh"

#include "xAODJet/Jet.h"

BTagInputChecker::BTagInputChecker():
  m_trackAssociator("BTagTrackToJetAssociator")
{
}

unsigned BTagInputChecker::nMissingTracks(const xAOD::Jet& jet) {
  const xAOD::BTagging *btagging = jet.btagging();
  if (!btagging) throw std::runtime_error("can't find btagging object");
  unsigned missing = 0;
  for (const auto &link : m_trackAssociator(*btagging)) {
    if(!link.isValid()) missing++;
  }
  return missing;
}

bool BTagInputChecker::hasTracks(const xAOD::Jet& jet) {
  return nMissingTracks(jet) == 0;
}

bool BTagInputChecker::hasBTagging(const xAOD::Jet& jet) {
  const xAOD::BTagging *btagging = jet.btagging();
  if (!btagging) return false;
  return true;
}
