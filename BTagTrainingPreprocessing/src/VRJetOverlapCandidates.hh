#ifndef VR_JET_OVERLAP_CANDIDATES_HH
#define VR_JET_OVERLAP_CANDIDATES_HH

#include "xAODJet/JetContainer.h"

enum class VRJetSelection {
  TWIKI, ALL
};

namespace detail {
  struct ORJet
  {
    ORJet(const TLorentzVector& p4);
    TLorentzVector p4;
    double radius;
  };
}

class VRJetOverlapCandidates
{
public:
  VRJetOverlapCandidates(const xAOD::JetContainer& jets,
                         VRJetSelection = VRJetSelection::ALL);
  double relativeDeltaR(const xAOD::Jet& jet) const;
private:
  std::vector<detail::ORJet> m_jets;
};

#endif
